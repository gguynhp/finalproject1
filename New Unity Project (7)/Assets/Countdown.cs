﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Countdown : MonoBehaviour {
    public Text t;
    public float n;
     
    void Update () {
        n -= Time.deltaTime;
        // t.text = n.ToString();
        t.text = System.Math.Round(n, 2).ToString(); //ทศนิยม 2 ตำแหน่ง
        t.text = Mathf.Round(n).ToString(); // ไม่ม่ทศนิยม

        if(n <=0)
        {
            t.text = "  Time Out";
        }
    }
}
